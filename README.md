# FUMiniHotelSystem

FUMiniHotelSystem is a Hotel Management System designed to streamline and automate various aspects of managing a hotel. It allows you to efficiently manage customer information, room information, and online/offline booking transactions.

## Key Features

- **Manage Customer Information:** Maintain a comprehensive database of customer information, including contact details, identification documents, and room booking history.

- **Manage Room Information:** Efficiently handle the details of rooms and their occupants in the hotel. This includes room availability, occupancy status, and any related information.

- **Manage Online/Offline Booking Transactions:** Facilitate the booking process for customers, whether online or offline. Provide real-time availability, rental rates, and instant booking confirmations.

## Default Admin Account

The application comes with a default admin account for initial setup:

- **Email:** admin@FUMiniHotelSystem.com
- **Password:** @@abc123@@

This admin account is configured in the appsettings.json file.

## Getting Started

To get started with FUMiniHotelSystem, follow these steps:

1. Clone the repository: `git clone <repository_url>`
2. Navigate to the project directory: `cd FUMiniHotelSystem`
3. Set up the database and migrations: `dotnet ef database update`
4. Configure the admin account in the appsettings.json file.
5. Run the application: `dotnet run`

## Usage

Describe here how users can interact with the system, such as creating and managing customer records, handling room information, and processing bookings.

## Contributing

If you would like to contribute to the development of FUMiniHotelSystem, please follow our [contribution guidelines](CONTRIBUTING.md).

## License

This project is licensed under the [MIT License](LICENSE).

